#!/bin/bash

IMAGE=srcco.de:3
docker build -t $IMAGE .
docker run -d --name srcco-de -u $(id -u) -v /var/www/srcco.de/workdir:/workdir --restart=always $IMAGE
