.. title: About
.. slug: about
.. date: 2014/01/21 21:15:13
.. tags:
.. link:
.. description:
.. type: text

This site was created by Henning Jacobs using Nikola_. Page background and header were drawn with water colors, scanned, and edited with GIMP_.
All content is written in VIM_.

There is not much to tell about myself. I like `Free Software`_ (aka Open Source), Kubernetes_, and Python_.
I'm currently employed at Zalando. You can learn more about my Zalando journey in my blog post `One Decade in Zalando Tech`_.

Note: I'm only running Ubuntu_/debian-based machines at the moment and simply assume the reader does so too.

.. _Nikola: https://getnikola.com
.. _GIMP: https://www.gimp.org
.. _VIM: https://www.vim.org
.. _Ubuntu: https://www.ubuntu.com
.. _Free Software: https://fsfe.org
.. _Kubernetes: /categories/kubernetes.html
.. _Python: /categories/python.html
.. _One Decade in Zalando Tech: /posts/one-decade-in-zalando-tech.html
