.. title: Xubuntu 20.04 LTS on Thinkpad L15
.. slug: installing-xubuntu-20-04-thinkpad-l15
.. date: 2021/02/07 16:42:00
.. tags: xubuntu
.. link:
.. description: Installing Xubuntu 20.04 LTS on the Lenovo Thinkpad L15
.. previewimage: ../galleries/lenovo-thinkpad-l15.png
.. type: text

.. image:: ../galleries/lenovo-thinkpad-l15.png
   :class: left

Installing Xubuntu 20.04 LTS on a new Lenovo Thinkpad L15 was relatively painless,
but involves a few steps. Especially the pre-installed Windows 10 makes it harder than necessary.

.. TEASER_END

I went for a new Lenovo Thinkpad L15 as it promised good quality and decent Linux support [#]_.
The "L" line is the cheaper version of the Thinkpad "T" models.
While "T" looks like the safe-bet in terms of hardware quality, I wanted to save some money and therefore decided for the L model
hoping for good-enough quality.

The Specs
=========

My Lenovo Thinkpad L15 (20U70006GE) features:

* AMD Ryzen 5 4500U processor with 6 cores
* 15" Full HD display
* 16 GB RAM
* 512 GB SSD

Installation Steps
==================

* `Download Xubuntu 20.04 LTS ISO <https://xubuntu.org/download>`_ and create USB boot stick via ``usb-creator-gtk``
* Set up pre-installed Windows 10 (and decline all Microsoft accounts and tracking)
* Windows update, update, update (and reboots) + install latest Lenovo BIOS (Windows software to update BIOS is pre-installed)
* Disable Secure Boot via BIOS in order to boot from Xubuntu USB stick
* Discover that Xubuntu cannot install next to Windows¹ because BitLocker is “active”
* Boot to Windows, enable BitLocker, reboot, disable BitLocker (BitLocker is in some weird “waiting for activation” state for the OEM install)
* Install Xubuntu
* Install newer Kernel via ``apt install linux-generic-hwe-20.04`` for better Hardware support (suspend has problems otherwise)
* Success 🎉

¹) I want to keep Windows installed “just in case” and for the Lenovo BIOS update tool.

It was a bit annoying jumping through those hoops with Windows and BitLocker.
I would have preferred to buy the laptop without any OS pre-installed, but apparently this is only available for students (“Lenovo Campus” line).

I don’t have any more experiences or problems to share so far. Everything I need works. I might report back after some weeks.

.. [#] I tried to find authoritative information on Linux support for the L15, but there are only pieces of information available, e.g. on https://www.lenovo.com/linux
