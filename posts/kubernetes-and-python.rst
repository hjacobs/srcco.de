.. title: Kubernetes and Python
.. slug: kubernetes-and-python
.. date: 2019/09/10 19:27:00
.. tags: kubernetes, python
.. link:
.. description:
.. previewimage: ../galleries/python-logo.png
.. type: text

.. image:: ../galleries/python-logo.png
   :class: left

While Go is the language-of-choice in the cloud-native world, Python has a huge community and makes it really easy to extend Kubernetes in only a few lines of code.
This post is a follow-up to a talk I gave last week in Prague.

.. TEASER_END

Marek_ invited me to give a talk at the `Cloud Native meetup in Prague <https://www.meetup.com/Cloud-Native-Prague/events/263802447/>`_. I chose "Kubernetes + Python = ❤" as a subject because I realized that I never talked
about my usage of Python for Kubernetes controllers/operators/apps. You can find `the slides of my talk on Slideshare <https://www.slideshare.net/try_except_/kubernetes-python-cloud-native-prague>`_ and relevant slides are linked in the text below.

.. image:: ../galleries/kubernetes-and-python/meetup-talk-photo1.jpg
   :alt: Cloud Native Prague meetup
   :target: ../galleries/kubernetes-and-python/meetup-talk-photo1.jpg
   :class: center

.. _Marek: https://twitter.com/MarekBartik

Running Python on Kubernetes
----------------------------

Let's start simple: how to run a Python web service on Kubernetes? I tend to use the new asyncio AIOHTTP_ framework, so `the code <https://codeberg.org/hjacobs/kubernetes-and-python/src/branch/master/01-running-python-on-kubernetes/aiohtttp-helloworld/web.py>`_ is less than 10 lines of Python and the Dockerfile is straightforward:

.. code-block:: Dockerfile

    FROM python:3.7-alpine

    RUN pip install aiohttp

    COPY web.py /

    ENTRYPOINT ["python", "web.py"]

Everybody "knows" that "Python is slow" due to its GIL_, but a non-scientific test (vegeta_) shows that it's **fast enough** and can sustain 200 rps with 4.8ms p99 latency (`slide 12`_).

There is one detail which makes the default behavior of ``aiohttp.web.run_app`` not safe to use: **Kubernetes' "graceful shutdown" behavior** (`slide 14`_).
AIOHTTP will by default handle the ``SIGTERM`` signal which leads to failing requests during Pod termination / rolling deployments.
Disabling the signal handling (thus relying on the ``KILL`` signal after 30 seconds) with ``web.run_app(app, handle_signals=False)`` will remove the failing requests as this test shows (`slide 15`_):

.. code-block:: text

    tests/e2e/test_update.py::test_rolling_update_no_signal_handling
    Elapsed:        41.60 seconds
    Successes:      2056
    Errors:         0

    tests/e2e/test_update.py::test_rolling_update_with_signal_handling
    Elapsed:        15.16 seconds
    Successes:      816
    Errors:         2

An alternative which works for any app is the **prestop trick**: add a ``preStop`` lifecycle command with "sleep 20" to your container (`slide 16`_).

.. _GIL: https://wiki.python.org/moin/GlobalInterpreterLock
.. _vegeta: https://github.com/tsenart/vegeta
.. _slide 12: https://www.slideshare.net/try_except_/kubernetes-python-cloud-native-prague/12
.. _slide 14: https://www.slideshare.net/try_except_/kubernetes-python-cloud-native-prague/14
.. _slide 15: https://www.slideshare.net/try_except_/kubernetes-python-cloud-native-prague/15
.. _slide 16: https://www.slideshare.net/try_except_/kubernetes-python-cloud-native-prague/16
.. _AIOHTTP: https://aiohttp.readthedocs.io/

You can find the complete `"hello world" aiohttp example in the repo <https://codeberg.org/hjacobs/kubernetes-and-python/src/branch/master/01-running-python-on-kubernetes/aiohtttp-helloworld>`_.

Accessing Kubernetes from Python
--------------------------------

.. image:: ../galleries/kubernetes-and-python/twitter-client-go-is-not-for-mortals.png
   :alt: client-go isn't for mortals - Bryan Liles
   :target: https://twitter.com/phennex/status/1131460883564118017
   :class: center

There are (at least) two Kubernetes clients for Python (`slide 24`_):

* `Official Kubernetes Python client`_: ``pip install kubernetes``
* `Pykube-NG`_: ``pip install pykube-ng``

.. _slide 24: https://www.slideshare.net/try_except_/kubernetes-python-cloud-native-prague/24
.. _Official Kubernetes Python client: https://github.com/kubernetes-client/python
.. _Pykube-NG: https://github.com/hjacobs/pykube

Both do their job, but (no surprise) I prefer Pykube-NG as it is more lightweight and feels more "Pythonic", compare yourself:

.. code-block:: python

    # Official Kubernetes Python client
    from kubernetes import client, config

    config.load_kube_config()
    v1 = client.CoreV1Api()
    ret = v1.list_namespaced_pod("foobar")
    for pod in ret.items:
        images = [c.image for c in pod.spec.containers]
        print(pod.metadata.name, ", ".join(images))

.. code-block:: python

    # Pykube-NG
    from pykube import HTTPClient, KubeConfig, Pod

    api = HTTPClient(KubeConfig.from_file())

    for pod in Pod.objects(api).filter(namespace="foobar"):
        images = [c["image"] for c in pod.obj["spec"]["containers"]]
        print(pod.name, ", ".join(images))

Pykube is more lightweight (160 KiB vs 23 MiB for the official client) and provides generic methods like ``obj.create()`` for Pods, Deployments, etc (the official client has methods for each OpenAPI operation).
This is helpful when writing generic controllers which have to deal with different kind of Kubernetes objects.

Writing a simple Kubernetes controller takes only a `few lines of Python code`_ (`slide 26`_) and I created a bunch of controllers this way:

* kube-downscaler_ to scale down deployments after work hours, e.g. to save cloud costs
* kube-janitor_ to clean up (delete) Kubernetes resources after a configured TTL (time to live), e.g. to automatically remove PR/preview deployments in test clusters


.. _few lines of Python code: https://codeberg.org/hjacobs/kubernetes-and-python/src/branch/master/03-writing-simple-controllers-with-python/scale-down-on-weekend/scale-down.py
.. _slide 26: https://www.slideshare.net/try_except_/kubernetes-python-cloud-native-prague/26
.. _kube-downscaler: https://github.com/hjacobs/kube-downscaler
.. _kube-janitor: https://github.com/hjacobs/kube-janitor

Writing Operators
-----------------

`Kubernetes Operators`_ can provide important functionality to integrate Kubernetes with other infrastructure or to run stateful services on Kubernetes.
Zalando open sourced two operators which are successfully used in critical production scenarios:

* `Postgres Operator`_ which creates and manages PostgreSQL clusters on Kubernetes using our battle-tested Patroni_
* `Elasticsearch Operator`_ which safely-drains and auto-scales Elasticsearch on Kubernetes

Both operators are written in Go and evolved over time. What is the best way to write operators in Python without boilerplate?
Zalando's answer is the `Kubernetes Operator Pythonic Framework (Kopf)`_! Kopf allows writing simple handler functions which are called automatically on create/update/delete for your CRD objects (`slide 40`_):

.. code-block:: python

    import kopf


    @kopf.on.create("example.org", "v1", "helloworlds")
    def on_create(spec, **kwargs):
        print(f"Create handler is called with spec: {spec}")
        return {"message": f"Hello {spec['name']}!"}


    @kopf.on.update("example.org", "v1", "helloworlds")
    def on_update(body, **kwargs):
        print(f"Update handler is called with body: {body}")

That's all we need for handling creates and updates of "HelloWorld" CRD objects.
You can find the `complete Kopf example in the repo`_.

`Michael Gasch did a more elaborate demo`_ (creating vSphere VMs from Kubernetes) with Kopf at VMWorld,
there is a `Medium article about Building a Kubernetes Operator in Python with Zalando's Kopf`_,
and you can `check out Kopf's extensive documentation`_.


.. _Kubernetes Operators: https://coreos.com/operators/
.. _Postgres Operator: https://github.com/zalando/postgres-operator
.. _Patroni: https://github.com/zalando/patroni/
.. _Elasticsearch Operator: https://github.com/zalando-incubator/es-operator
.. _Kubernetes Operator Pythonic Framework (Kopf): https://github.com/zalando-incubator/kopf
.. _complete Kopf example in the repo: https://codeberg.org/hjacobs/kubernetes-and-python/src/branch/master/04-writing-operators-with-python/kopf-example
.. _Michael Gasch did a more elaborate demo: https://twitter.com/embano1/status/1157305880918011906
.. _Medium article about Building a Kubernetes Operator in Python with Zalando's Kopf: https://medium.com/swlh/building-a-kubernetes-operator-in-python-with-zalandos-kopf-37c311d8edff
.. _check out Kopf's extensive documentation: https://kopf.readthedocs.io/


Testing with Pytest and Kind
----------------------------

Writing apps/controllers/operators for Kubernetes obviously requires some testing!
The tool of choice for this is kind_ (Kubernetes IN Docker) as it has no dependencies (except Docker) and
provides a tight feedback loop (cluster bootstrap is fast).

I created pytest-kind_, a small plugin for pytest to integrate with kind (`slide 19`_). It will download kind, create a cluster, and provide access via a pytest fixture.
A complete test for our "hello world" Kopf operator does not require much:

.. code-block:: python

    import requests
    import time

    from pykube.objects import NamespacedAPIObject


    class HelloWorld(NamespacedAPIObject):
        version = "example.org/v1"
        kind = "HelloWorld"
        endpoint = "helloworlds"


    def test_web_hello_world(kind_cluster):
        kind_cluster.load_docker_image("kopf-example")
        kind_cluster.kubectl("apply", "-f", "deploy/")
        kind_cluster.kubectl("rollout", "status", "deployment/kopfexample-operator")
        kind_cluster.kubectl("apply", "-f", "example.yaml")

        for i in range(10):
            obj = HelloWorld.objects(kind_cluster.api).get(name="test-1")
            if "status" in obj.obj:
                break
            time.sleep(2)

        assert obj.obj["status"]["on_create"]["message"] == "Hello Prague!"


Even my `recently introduced`_ frontend project Kubernetes Web View (`slide 47+`_) is easy to test as it relies on plain HTML rendering (using Jinja2_)
and therefore does not require sophisticated browser-based testing (`slide 67`_).


.. image:: ../galleries/kubernetes-and-python/kube-web-view-slide-61.png
   :alt: Kubernetes Web View: Label and Custom Columns
   :target: https://www.slideshare.net/try_except_/kubernetes-python-cloud-native-prague/61
   :class: center


.. _kind: https://kind.sigs.k8s.io/
.. _Jinja2: https://palletsprojects.com/p/jinja/
.. _recently introduced: https://srcco.de/posts/kubernetes-web-uis-in-2019.html
.. _slide 19: https://www.slideshare.net/try_except_/kubernetes-python-cloud-native-prague/19
.. _slide 40: https://www.slideshare.net/try_except_/kubernetes-python-cloud-native-prague/40
.. _slide 47+: https://www.slideshare.net/try_except_/kubernetes-python-cloud-native-prague/47
.. _slide 67: https://www.slideshare.net/try_except_/kubernetes-python-cloud-native-prague/67



Kubernetes + Python = ❤
-----------------------

It's pretty clear that I like Kubernetes and Python, a quick look at my open source projects is enough:

* kube-downscaler_: uses pykube-ng_ to downscale deployments after working hours
* kube-janitor_: uses pykube-ng_ to clean up resources with a given TTL
* kube-ops-view_: uses pykube-ng_ to query the Kubernetes API and render a WebGL view
* kube-resource-report_: uses pykube-ng_ to collect resource requests/usage and render HTML pages with Jinja2
* kube-web-view_: uses pykube-ng_ and AIOHTTP_ to provide a "kubectl for the web" experience
* pykube-ng_: a lightweight Python client for Kubernetes
* pytest-kind_: pytest plugin to test Python apps/operators with KIND

I feel rather productive with my personal Python+Kubernetes setup, what are your experiences?

Do you write any Kubernetes applications/controllers/operators in Python?
Do you use any of the mentioned open source projects? `Let me know on Twitter!`_

.. _kube-ops-view: https://github.com/hjacobs/kube-ops-view
.. _kube-resource-report: https://github.com/hjacobs/kube-resource-report
.. _kube-web-view: https://codeberg.org/hjacobs/kube-web-view
.. _pytest-kind: https://codeberg.org/hjacobs/pytest-kind
.. _Let me know on Twitter!: https://twitter.com/try_except_
