.. title: KubeCon Barcelona Recap
.. slug: kubecon-barcelona-2019-recap
.. date: 2019/05/30 10:40:00
.. tags: kubernetes
.. link:
.. description:
.. previewimage: ../galleries/kubecon-barcelona-2019.png
.. type: text

.. image:: ../galleries/kubecon-barcelona-2019.png
   :class: left

`KubeCon EU <https://events.linuxfoundation.org/events/kubecon-cloudnativecon-europe-2019/>`_ happened in Barcelona this year (May 20 - 23). This is my personal conference recap.
I will not talk about any of the official KubeCon announcements (as there are enough other places to read about them).

.. TEASER_END

Pre-Conference Events
---------------------

CNCF End User Partner Summit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

I flew in on Sunday and attended the CNCF End User Partner Summit on Monday.
The goal was to meet people and companies in the End User Community face to face in a vendor-neutral zone.
Spotify and Adidas presented their cloud-native journeys before lunch. The afternoon was reserved for unconference topics.
I joined two round tables: (1) SIG Developer Experience and (2) "PowerUser tools" (notes are in the `private CNCF enduser repo <https://github.com/cncf/enduser/tree/master/kubecon>`_, only accessible by End User companies).

Here the "PowerUser tools" we briefly talked about at the second round table:

* kubectx_ / kubens
* Argo_ by Intuit --- workflow tool / operator "fancy Kubernetes job"
* Cloud Code IDE plugin --- 2 users (OK, one install at least)
* stern_ --- log tailing
* kail_ --- CLI tool which makes it easy to get logs (easier to use than kubectl logs ..)
* Kubernetes RDS operator --- cloud agnostic database
* kubefwd_ --- make services available locally, forward a whole namespace
* Kubernetes Dashboard --- people using it, but features lacking (also lacks understanding of "applications")
* krew_ --- nobody really using it
* k8sec_ --- for managing secrets, e.g. list secrets which are used by a pod
* `kube-ops-view <https://github.com/hjacobs/kube-ops-view>`_ --- killing Raspberry PI
* `Kubernetes Resource Report <https://github.com/hjacobs/kube-resource-report>`_ --- static HTML report for resources
* `kube-downscaler <https://github.com/hjacobs/kube-downscaler/>`_ --- downscale during off-hours
* `kube-janitor <https://github.com/hjacobs/kube-janitor>`_ --- housekeeping for your cluster
* k9s_ --- interactive CLI tool, helpful in stress/incident situations
* kube-shell --- does not work properly
* `kube-ps1 <https://github.com/jonmosco/kube-ps1>`_ --- showing current context
* `click <https://github.com/databricks/click>`_ --- did not get it to work
* kube-notify --- if pod switching to state, send a notification
* Sentry for Kubernetes events --- not really known/used
* Heptio Sonobuoy_

.. _Argo: https://argoproj.github.io/
.. _kubectx: https://github.com/ahmetb/kubectx
.. _stern: https://github.com/wercker/stern
.. _kail: https://github.com/boz/kail
.. _kubefwd: https://kubefwd.com/
.. _krew: https://github.com/kubernetes-sigs/krew
.. _k9s: https://github.com/derailed/k9s
.. _k8sec: https://github.com/dtan4/k8sec
.. _Sonobuoy: https://github.com/heptio/sonobuoy

My take-away is that the most used and liked tools are the basic CLI helpers like kube-ps1, kubens, and stern_/kail_.
Note that the list is ordered by time it came up during the discussion, if you know other valuable "PowerUser" tools, `please let me know <https://twitter.com/try_except_>`_.

I really like the CNCF End User community as it allows to exchange experiences with organizations on a similar journey.
While companies and industries are different, there are enough touch points to talk about challenges, e.g. with regards to cloud native developer experience.
Thanks to Cheryl_ for organizing the face-to-face meeting!

AWS Birds-of-Feather
^^^^^^^^^^^^^^^^^^^^

AWS and Weaveworks `organized a Birds-of-Feather on Monday evening <https://discuss.kubernetes.io/t/call-for-aws-birds-of-feather-at-kubecon-barcelona/6173>`_. A few lightning talks (I also did one about `kube-resource-report <https://github.com/hjacobs/kube-resource-report>`_) were followed by a panel discussion with AWS, Weaveworks, and users.
I don't remember all the details, but it seemed that there are still enough open questions around how to set up and maintain clusters on AWS.


KubeCon Talks
-------------

I attended very few breakout sessions. I only watched the keynotes, gave `my own talk`_ (to 1500 people!), and attended the following sessions:

* `Istio, We Have a Problem! Understanding and Fixing Bugs with a Service-Mesh <https://www.youtube.com/watch?v=9CQ0PMiOGhg>`_: only attended because it was right before `my own talk`_
* `Es operator: Building an Elasticsearch Operator From the Bottom Up <https://www.youtube.com/watch?v=lprE0J0kAq0>`_: how Zalando runs Elasticsearch on Kubernetes
* `10 Ways to Shoot Yourself in the Foot with Kubernetes, #9 Will Surprise You - Datadog <https://www.youtube.com/watch?v=QKI-JRs2RIE>`_: another failures talk by Datadog, **recommended**
* `Building and Maintaining a Client Library - Stories From the Trenches <https://www.youtube.com/watch?v=uCgFETEdC80>`_: some interesting insights on how tricky the Kubernetes API and clients can be
* `Kubernetes Networking at Scale <https://www.youtube.com/watch?v=MvoImel5qfc>`_: good stuff on DNS, IPVS, routing, VPC CNI, etc

.. _my own talk: https://www.youtube.com/watch?v=6sDTB4eV4F8

I really liked that we had multiple failure talks this time (incl. `Spotify's keynote`_), so I could add four more entries to https://k8s.af

.. _Spotify's keynote: https://www.youtube.com/watch?v=ix0Tw8uinWs

Most of my time I spent in the booth area and talked with people (the "hallway track").

Hallway Track
-------------

All KubeCon `talks are recorded <https://www.youtube.com/playlist?list=PLj6h78yzYM2PpmMAnvpvsnR4c27wJePh3>`_, so I planned to bet on the hallway track and meet as many people as possible.
I would definitely recommend this to all conference attendees:

.. image:: ../galleries/twitter-kubeconeu-2019-1-1.png
   :class: center
   :target: https://twitter.com/try_except_/status/1131312122053505024

Some of the things I learned:

* K8Spin_ uses my kube-janitor_ to delete namespaces automatically
* mytaxi disabled CPU throttling (`like Zalando <https://www.slideshare.net/try_except_/optimizing-kubernetes-resource-requestslimits-for-costefficiency-and-latency-jax-devops-london>`_) and `others are now doing the same <https://twitter.com/it_supertramp/status/1133648291332263936>`_
* there are a few new (?) vendors offering canary deployment and "developer experience" tooling (I won't advertise for them here, but I found some of their approaches interesting to learn from)
* official or inofficial stuff about AWS infrastructure (and some horror stories to not disclose) --- one quote was "EKS support sucks"
* how people do Kubernetes cost management (in short: most don't)
* where Zalando OSS tools like `External DNS`_, Skipper_, and `AWS Ingress Controller`_ are used in production
* tips & tricks on how to advocate for Kubernetes in the company
* that teutoStack_ uses Zalando's `Postgres Operator`_ for their `managed Postgres-as-a-Service (on premise) <https://twitter.com/try_except_/status/1130875778730778625>`_
* some Kubernetes failure stories (where I hopefully get a few more pointers from the people who told them to me)
* that `Steve Wade`_ has some Istio failure stories to share (Steve, I will remind you about this..)


.. _K8Spin: https://k8spin.cloud/
.. _kube-janitor: https://github.com/hjacobs/kube-janitor
.. _External DNS: https://github.com/kubernetes-incubator/external-dns
.. _Skipper: https://github.com/zalando/skipper/
.. _AWS Ingress Controller: https://github.com/zalando-incubator/kube-ingress-aws-controller
.. _teutoStack: https://teuto.net/
.. _Postgres Operator: https://github.com/zalando/postgres-operator
.. _Steve Wade: https://twitter.com/swade1987


A few of the people I met at KubeCon (feel free to follow them on Twitter):

* `Georges Chaudy <https://twitter.com/GeorgesChaudy>`_
* `Joel Speed <https://twitter.com/JoelASpeed>`_
* `Kasper Nissen <https://twitter.com/phennex>`_
* `Laurent Bernaille <https://twitter.com/lbernail>`_
* `Lili <https://twitter.com/LiliCosic>`_
* `Marco Pracucci <https://twitter.com/pracucci>`_
* `Marek Bartík <https://twitter.com/MarekBartik>`_
* `Misterious Observer <https://twitter.com/MstrsObserver>`_
* `Pau Rosello <https://twitter.com/paurosello>`_
* `piontec <https://twitter.com/piontec>`_
* `Puja Abbassi <https://twitter.com/puja108>`_
* `Robert Boll <https://twitter.com/roboll_>`_
* `Sebastian Herzberg <https://twitter.com/shrzbrg>`_
* `Seth McCombs <https://twitter.com/SethMcCombs>`_
* `Suhail Patel <https://twitter.com/suhailpatel>`_
* `Thomas Peitz <https://twitter.com/it_supertramp>`_

And of course my Zalando colleagues:

* `Diego Roccia <https://twitter.com/rocciadiego>`_
* `Jannis Rake-Revelant <https://twitter.com/jannis_r>`_
* `Mikkel Oscar Lyderik Larsen <https://twitter.com/mikkeloscar>`_

The hallway track and conference is best ended with a nice post-KubeCon social!

.. image:: ../galleries/twitter-kubeconeu-2019-post-social.png
   :class: center
   :target: https://twitter.com/milesbxf/status/1131619197925113857



Summary
-------

I did not mention the awesome party, won't complain much about the venue (far from optimal, long distance walks, distracting noise in 8.1), the not-so-good food, and the enormous amounts of produced garbage.
**KubeCon is all about meeting people and exchanging ideas & experiences** --- in this sense KubeCon Barcelona was a huge success for me (as always)!

Cheryl_ asked for KubeCon feedback in the CNCF End User retrospective call: there were many votes for some form of facilitation for the hallway track, e.g. whiteboards, app to connect people, and similar.
For the next CNCF event, I hope we will see some official facilitation for the hallway track to make it easier to connect with people (7000+!).

I started a `thread in the community forums to collect KubeCon recaps and talk recommendations <https://discuss.kubernetes.io/t/cloud-native-barcelona-kubecon-eu-recaps-talk-recommendations/6564>`_, please contribute!
I definitely want to hear your talk recommendations (as I haven't watched many talks myself yet).

.. _Cheryl: https://twitter.com/oicheryl
