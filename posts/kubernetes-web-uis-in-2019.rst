.. title: Kubernetes Web UIs in 2019
.. slug: kubernetes-web-uis-in-2019
.. date: 2019/08/12 22:24:00
.. tags: kubernetes
.. link:
.. description:
.. previewimage: ../galleries/kubernetes-web-uis/kube-web-view-1.png
.. type: text

.. image:: ../galleries/kubernetes-web-uis/kube-web-view-1.png
   :class: left

This post takes a look at different open source Kubernetes web UIs, my requirements, and why I created `Kubernetes Web View <https://codeberg.org/hjacobs/kube-web-view/>`_ to help with support and troubleshooting across multiple clusters.

.. TEASER_END

Use Cases
---------

At Zalando, we have a large number of Kubernetes users (900+) and clusters (100+). There are two main use cases where I would wish for some web tool support: 1) support interactions with colleagues 2) incident response and investigation.

Support
^^^^^^^

I observed that support interactions in chat often look like:

* A: Help our service XYZ is not reachable
* B: What do you see when you do ``kubectl describe ingress ...``?

Or similar for CRDs:

* A: I am facing an issue with identity service, ...
* B: What is the output of ``kubectl describe platformcredentialsset ...``?

These interactions follow a pattern of using the ``kubectl`` command line to investigate and pinpoint a problem.
This requires context switches on both sides: participants have to switch between terminal and web chat, plus they don't see the same situation.

I would wish for a Kubernetes web frontend to help with this:

* users **can share links** and see the same situation
* **avoid human mistakes** during support: login to wrong cluster on the command line, typos in CLI commands, etc
* **allow crafting custom views** to send to colleagues, i.e. add label columns, show multiple resource types on one page
* ideally the web tool should allow **deep links into specific YAML sections** (e.g. to point to a wrong spec causing the issues)


Incident Response & Investigation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Incident response for infrastructure requires gaining situational awareness, the ability to assess impact, and finding patterns across clusters.
Some real-life example situations:

* a critical production service has problems, you need to **find the Kubernetes resources by name across all clusters** in order to help troubleshoot
* your nodes start failing on scale-up and you need to **find all "Pending" Pods across all clusters** to assess the impact
* some users report an issue for a DaemonSet which is deployed in all clusters, you need to find **whether there is a problem across all clusters**

My standard solution for this tasks is something like: ``for i in $clusters; do kubectl ...; done`` --- obviously there could be a web tool providing similar functionality.

Existing Kubernetes Web UIs
---------------------------

The open source world of Kubernetes web frontends is not very big [#]_, so I tried to gather more input on Twitter:

.. image:: ../galleries/kubernetes-web-uis/twitter-kubernetes-web-uis-2019-07.png
   :class: center
   :target: https://twitter.com/try_except_/status/1153543128365850625

This is how I learned about K8Dash_, Kubernator_, and Octant_.
Let's have a look at current open source Kubernetes Web UIs and how they might help:

K8Dash
------

    *"K8Dash is the easiest way to manage your Kubernetes cluster."*

.. image:: ../galleries/kubernetes-web-uis/k8dash.png
   :alt: K8Dash
   :target: ../galleries/kubernetes-web-uis/k8dash.png
   :class: center

K8Dash_ looks pretty nice and feels fast, but has some shortcomings for my use cases:

* Single cluster only
* Sorting and filtering is possible, but has no permalinks
* No support for Custom Resource Definitions (CRDs)



Kubernator
----------

    *“Kubernator is an alternative Kubernetes UI. In contrast to high-level Kubernetes Dashboard, it provides low-level control and clean view on all objects in a cluster with the ability to create new ones, edit and resolve conflicts. As an entirely client-side app (like kubectl), it doesn’t require any backend except Kubernetes API server itself, and also respects cluster’s access control.”*

.. image:: ../galleries/kubernetes-web-uis/kubernator.png
   :alt: Kubernator
   :target: ../galleries/kubernetes-web-uis/kubernator.png
   :class: center

That's a pretty accurate description for Kubernator_. Missing points for the described support & incident response use cases:

* Single cluster only
* No list view (i.e. no way to list all pending pods)


Kubernetes Dashboard
--------------------

    *“Kubernetes Dashboard is a general purpose, web-based UI for Kubernetes clusters. It allows users to manage applications running in the cluster and troubleshoot them, as well as manage the cluster itself.”*

.. image:: ../galleries/kubernetes-web-uis/kubernetes-dashboard.png
   :alt: Kubernetes Dashboard UI
   :target: ../galleries/kubernetes-web-uis/kubernetes-dashboard.png
   :class: center

`Kubernetes Dashboard`_ will sadly not really help with my support & incident response use cases:

* No permalinks, e.g. when I filter resources or when I change the sorting.
* No easy way to filter by status, e.g. to see all "Pending" pods.
* Only support for a single cluster.
* No support for CRDs (this is being worked on)
* No custom columns (e.g. label columns like ``kubectl -L``).

Kubernetes Operational View (kube-ops-view)
-------------------------------------------

    *“Read-only system dashboard for multiple K8s clusters”*

.. image:: ../galleries/kubernetes-web-uis/kube-ops-view.png
   :alt: kube-ops-view
   :target: ../galleries/kubernetes-web-uis/kube-ops-view.png
   :class: center

`Kubernetes Operational View`_ has a completely different approach: it only shows cluster nodes and pods rendered via WebGL and has no textual object details.
It's good to get a quick overview of a cluster state ("pods crashing?"), but does not really help much in the described support and incident response use cases.

Kubernetes Resource Report (kube-resource-report)
-------------------------------------------------

   *“Report Kubernetes cluster and pod resource requests vs usage and generate static HTML”*

.. image:: ../galleries/kubernetes-web-uis/kube-resource-report.png
   :alt: kube-resource-report
   :target: ../galleries/kubernetes-web-uis/kube-resource-report.png
   :class: center

`Kubernetes Resource Report`_ generates static HTML reports to report resource usage and cost attribution per team/application across clusters.
The report helps to a limited degree also for support and incident response as it allows to quickly find the cluster where an application is deployed to.

Octant
------

    *“A web-based, highly extensible platform for developers to better understand the complexity of Kubernetes clusters.”*

.. image:: ../galleries/kubernetes-web-uis/octant.png
   :alt: Octant UI
   :target: ../galleries/kubernetes-web-uis/octant.png
   :class: center

VMWare's Octant_ is fairly new and I only learned about it recently. It's pretty nice to explore a single cluster from the local machine (even comes with visualization), but it only covers support & incident response to some extend:

* No search across clusters
* To be used on local machine (not deployed to cluster)
* No sorting/filtering of objects (only label selector is supported)
* No custom columns
* No listing of objects across namespaces

I also had problems getting Octant to work with Zalando clusters: it `crashed on certain CRDs <https://github.com/vmware/octant/issues/72>`_.


Introducing Kubernetes Web View
--------------------------------

    *"kubectl for the web"*

.. image:: ../galleries/kubernetes-web-uis/kube-web-view-1.png
   :alt: Kubernetes Web View
   :target: ../galleries/kubernetes-web-uis/kube-web-view-1.png
   :class: center

Looking at the UI options, I decided to create a new web frontend for Kubernetes: `Kubernetes Web View`_. I really just want a way of translating the power of ``kubectl`` to the web:

* enable all (read-only) operations where people commonly use ``kubectl`` as their tool of choice
* all URLs should represent the full view state (permalinks) in order to make them shareable among colleagues and facilitate deep-linking from other tools
* all Kubernetes objects should be supported to be able to troubleshoot any kind of problem
* resource lists should be easily downloadable for further processing (spreadsheet, CLI tools like ``grep``) and storage (e.g. for postmortems)
* selecting resources by label (similar to ``kubectl get .. -l``) should be supported
* composing views of different resource types should be possible (similar to ``kubectl get all``) to provide a common operational picture among colleagues (e.g. during incident response)
* adding custom "smart" deep links to other tools such as monitoring dashboards, logging providers, application registries, etc should be possible to facilitate troubleshooting and incident response
* keep the frontend as simple as possible (pure HTML) to avoid accidental problems, e.g. unresponsive JavaScript
* support multiple clusters to streamline discovery in on-call situations (only one entry URL to remember)
* facilitate ad-hoc analysis where possible (e.g. with download links for resources across clusters/namespaces)
* provide additional deep-linking and highlighting, e.g. to point colleagues to a certain part of a resource spec (line in YAML)
* allow customization for org-specific optimizations: e.g. custom view templates for CRDs, custom table views, custom CSS formatting
* provide means to continue investigation on the command line (e.g. by showing full ``kubectl`` command lines to copy)

Out-of-scope (non-goals) for Kubernetes Web View are:

* abstracting Kubernetes objects
* application management (e.g. managing deployments, Helm Charts, etc)
* write operations (this should be done via safe CI/CD tooling and/or GitOps)
* fancy UI (JavaScript, theming, etc)
* visualization (check out `kube-ops-view <https://github.com/hjacobs/kube-ops-view>`_)
* cost analysis (check out `kube-resource-report <https://github.com/hjacobs/kube-resource-report/>`_)

How does Kubernetes Web View help with support and incident response?

Support
^^^^^^^

* all links are **perma-links** making it easy to share views with colleagues
* **custom views** can be crafted, e.g. to show all Deployments and Pods with a certain label in two specific clusters (multiple cluster names and resource types can be separated by comma in the URL path)
* individual lines in the object YAML can be linked to point to potential problems in the object spec

.. image:: ../galleries/kubernetes-web-uis/kube-web-view-search.png
   :alt: Kubernetes Web View Search across clusters
   :target: ../galleries/kubernetes-web-uis/kube-web-view-search.png
   :class: center

Incident Response
^^^^^^^^^^^^^^^^^

* the **global search** allows finding objects across all clusters
* **list views** can show all objects with a certain state/column across all clusters (e.g. to find all pending pods)
* object **lists can be downloaded** as tab-separated values (TSV) for further analysis
* `customization with external links <https://kube-web-view.readthedocs.io/en/latest/customization.html#external-links>`_ allows fast jumps to relevant monitoring dashboards and other tools

.. image:: ../galleries/kubernetes-web-uis/kube-web-view-pending-pods.jpg
   :alt: Kubernetes Web View: listing all pending pods across clusters
   :target: ../galleries/kubernetes-web-uis/kube-web-view-pending-pods.jpg
   :class: center

If you want to try out Kubernetes Web View, `check out the documentation <https://kube-web-view.readthedocs.io/>`_ or `try the live demo <https://kube-web-view.demo.j-serv.de/>`_.

The UX can certainly be improved and Kubernetes Web View is currently a tool for "power users" who don't shy away from manipulating the URL path if necessary ;-)

If you have any feedback or improvement suggestions, `please reach out to me on Twitter <https://twitter.com/try_except_>`_!

This post should just provide a brief background on the journey leading to Kubernetes Web View. More updates to come!

UPDATE 2019-09-06
-----------------

My meetup talk about Kubernetes + Python = ❤ (see also the `follow-up blog post <https://srcco.de/posts/kubernetes-and-python.html>`_) shows `some features of Kubernetes Web View <https://www.slideshare.net/try_except_/kubernetes-python-cloud-native-prague/47>`_.

UPDATE 2019-09-19
-----------------

I learned about another Kubernetes desktop UI (Electron app) today: `Kubeman <https://github.com/walmartlabs/kubeman>`_  is "a tool that attempts to make it easier to find information from Kubernetes clusters, and to investigate issues related to Kubernetes and Istio."
While Kubeman does not provide the features I need (deployment as web app, permalinks, .), Kubeman has nice features I will `try to learn from <https://codeberg.org/hjacobs/kube-web-view/issues/83>`_: it comes with recipes (plugins) and allows comparing resources across clusters.


.. [#] My explanation for the small world of Kubernetes web UIs: cloud providers and Kubernetes vendors have their own proprietary web frontends and therefore there is only a limited space for "good" open source Kubernetes UIs.

.. _K8Dash: https://github.com/herbrandson/k8dash
.. _Kubernator: https://github.com/smpio/kubernator
.. _Kubernetes Dashboard: https://github.com/kubernetes/dashboard
.. _Kubernetes Operational View: https://github.com/hjacobs/kube-ops-view
.. _Kubernetes Resource Report: https://github.com/hjacobs/kube-resource-report/
.. _Octant: https://github.com/vmware/octant

