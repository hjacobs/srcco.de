.. title: Secure Scuttlebutt - SSB
.. slug: secure-scuttlebutt-ssb
.. date: 2021/01/30 18:15:00
.. tags: ssb
.. link:
.. description: Secure Scuttlebutt (SSB) is a Free-as-in-Freedom decentralized (p2p) social network.
.. previewimage: ../galleries/ssb/hermies.jpg
.. type: text

.. image:: ../galleries/ssb/hermies.jpg
   :class: left

Secure Scuttlebutt (SSB) is a Free-as-in-Freedom decentralized (p2p) social network.
I joined the "Scuttleverse" last December.
The Scuttleverse is a strange place. Strange, but friendly and welcoming.

.. TEASER_END

Secure Scuttlebutt is completely decentralized and offline-first.
Each SSB participant (account) has a feed with signed messages (sigchain) which can be pulled by others.
Feeds are immutable, i.e. SSB posts cannot be deleted or edited.
"Following" somebody means replicating their complete feed to the local disk. Thus all SSB content is stored distributed on participants' local drives.
You can post a message to your own feed without being online (local/offline-first) and it will be replicated as soon as your follower(s) connect.
Connections can be made locally (LAN/WiFi) without intermediaries or via so-called "rooms". Rooms provide connectivity for different SSB participants behind NAT.

There are a number of `different SSB clients <https://handbook.scuttlebutt.nz/applications>`_, the most prominent being "Patchwork" for Linux, macOS, and Windows.
Patchwork provides a friendly view of your timeline, supports "likes", hashtags as topic channels, and more:

.. image:: ../galleries/ssb/patchwork-screenshot.jpg
   :class: center
   :alt: Screenshot of the Patchwork SSB client

The Social
==========

The Scuttleverse is a friendly and welcoming place. Some SSB community gardening practices [#]_ contribute to this:

* welcoming newbies in the #new-people channel - this makes people feel at home, and connects them to interesting stuff happening in the Scuttleverse
* adding explanations when blocking to give other people context (“this account is posting racist memes”, “I only want people I’ve met in person in my feed”)
* reminding folks not to call each other names or troll
* flagging/blocking accounts that seem to be spreading toxic content
* explaining things patiently
* interrupting and disengaging from racist, trans-phobic, or anti-gay content
* hosting conversations on interesting topics
* trying to cheer up people having a bad day
* building relationships offline and in other online spaces
* posting in languages other than English
* reminding each other that our experiences are not universal and may have been biased by various forms of privilege

The Scuttleverse has no central authority to block or deplatform anyone. No single entity has this power in SSB, but it's evenly distributed to all peers [#]_.
You decide which feed to replicate. You decide which account to ignore or block. That's called "free listening".
Blocking someone is not considered harmful in SSB and can have a multitude of reasons.
Good practice is to provide context for the block so your followers can decide whether they want to block the account, too:

.. image:: ../galleries/ssb/block-reason.png
   :class: center
   :alt: Screenshot of providing a block reason on SSB

There are some other contributing factors which I think make the Scuttleverse special and maybe less toxic than Twitter, Mastodon & co:

* thinking twice before posting: all messages are immutable and SSB clients encourage checking before posting by providing the extra "preview & publish" step
* SSB is "slow" by design: messages might be seen only hours later when others are online to replicate them
* lack of amplification: SSB has no repost or retweet functionality, there is no way to gain wide reach without organic growth --- toxic content cannot go viral
* organic network of people: content only replicates to friends of friends by default, it's impractical to follow thousands of people in SSB --- `Dunbar's number <https://en.wikipedia.org/wiki/Dunbar%27s_number>`_ applies
* no ML recommendations: SSB clients won't suggest content to engage with, there is no entity to gain anything from more "clicks"

Onboarding
==========

Onboarding to the decentralized SSB is a bit tricky as you need to know someone on SSB to connect to.
Fortunately, there are so-called "Pubs" which are "always-on" accounts which help with bootstrapping.
`Public Pubs <https://github.com/ssbc/ssb-server/wiki/Pub-Servers>`_ provide free invite codes. Redeeming the invite code makes the Pub follow your feed thus others on the Pub can see your posts.
Joining a Pub is great to start from zero, but also means that your SSB client will pull a lot of data (Gigabytes) from the Pub.
Your SSB client might therefore be busy downloading and indexing for the first hours.

How to get started:

* download a SSB client, e.g. `Patchwork <https://github.com/ssbc/patchwork/>`_ or one of `the other apps <https://scuttlebutt.nz/get-started/>`_
* select a Pub from the `list of public Pubs <https://github.com/ssbc/ssb-server/wiki/Pub-Servers>`_
* copy the invite code
* open the SSB client, click on "+ Join Server", and paste the invite code
* wait for downloading and indexing (this might take hours!)
* follow people to your liking
* introduce yourself in the "#new-people" channel (can be later, lurking is fine!)
* optional: join one of the `public SSB rooms <https://github.com/ssbc/ssb-server/wiki/%23ssbrooms#public-rooms>`_ for better connectivity (or `join mine <http://ssb-room.j-serv.de>`_)
* optional: unfollow the Pub (the Pub will still follow you) [#]_

If you want to follow me on SSB, here my different identities (different devices):

* ``@+pY9goiTAmglEUMx+eXx2MpRExAY73C3RVGPy7QxrqQ=.ed25519``
* ``@3flA+1WgODxxRXljGHEbBLOPPMxg/AMaTvo7nE29TmM=.ed25519``
* ``@smIVNXkXDrRANTxq5ecgqHfN+8w85Ru2HNlCNL2DwDM=.ed25519``

Remember: everything in SSB is public [#]_ and you need to follow accounts to see content.

Further Information
===================

* `Secure Scuttlebutt homepage <https://scuttlebutt.nz/>`_
* `Free Social Networking Showdown - Scuttlebutt - blog post <https://john.colagioia.net/blog/media/2020/01/25/scuttlebutt.html>`_
* `Secure Scuttlebutt: Peer-to-peer collaboration and community infrastructure - Charles Lehner @ LibrePlanet 2020 <https://media.libreplanet.org/u/libreplanet/m/secure-scuttlebutt-peer-to-peer-collaboration-and-community-infrastructure/>`_
* `Building Blocks of Decentralization by Will Scott - rC3 talk <https://media.ccc.de/v/rc3-11400-building_blocks_of_decentralization>`_
* `Scuttlebutt Protocol Guide <https://ssbc.github.io/scuttlebutt-protocol-guide/>`_
* `Hacker News comment thread on SSB <https://news.ycombinator.com/item?id=25713830>`_

.. [#] Check the "#community-gardening" channel on SSB or directly this post (copy the message ID into your client search box): ``%6aVAYuUav2g/mkwG7y2Rs474G+TFiTUFhqMgUem3+uY=.sha256``
.. [#] See "Protecting SSB from yuge orange face or anyone like him" by André Staltz on SSB ``%LKqAORPSwWHNrEzQGgXRVGzmy8xOej71685p26a6PSg=.sha256``
.. [#] Pubs are great for onboarding, but Pubs are technically not required and following a public Pub might replicate content you don't want.
.. [#] There are also `private messages <https://ssbc.github.io/scuttlebutt-protocol-guide/#private-messages>`_ which are encrypted so only the recipients can read them.
