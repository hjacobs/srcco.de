.. title: My Favorite F-Droid Apps
.. slug: my-favorite-f-droid-open-source-android-apps-2019
.. date: 2019/12/15 10:40:00
.. tags: fdroid
.. link:
.. description:
.. previewimage: ../galleries/f-droid/f-droid-logo.png
.. type: text

.. image:: ../galleries/f-droid/f-droid-logo.png
   :class: left

I try to use Open Source Android apps whereever I can.
F-Droid_ is a community-maintained software repository which contains only free software apps.
This posts describes my favorite F-Droid apps.

.. TEASER_END

Apps are listed in alphabetical order.

2048
----

The game 2048_. One of the few mobile games I (rarely) use when bored.

Amaze
-----

Amaze_ is a neat file manager which is easier to use and has more features than the default "Files" application. Recommended.

.. image:: ../galleries/f-droid/amaze.png
   :alt: Amaze file manager
   :target: ../galleries/f-droid/amaze.png
   :class: center

Delta Chat
----------

`Delta Chat`_ is a chat client which relies completely on e-mail! This means you can chat with Delta Chat users and non-Delta users will get the chat message as e-email.

DNS66
-----

I only learned recently about DNS66_ which allows blocking arbitrary DNS hostnames e.g. to block ad servers.
It does not require Android root access and comes with preconfigured hosts lists (known ad servers).

GitNex
------

GitNex_ is a client for Gitea. Codeberg.org is powered by Gitea, so I can use GitNex to create and manage issues for my
open source projects hosted on codeberg.org.

GPSTest
-------

GPSTest_ does what the name says: you can test your GPS reception and view available satellites.
I use GPSTest mostly to see horizontal and vertical accuracy to check how far off my displayed map location could be.

.. image:: ../galleries/f-droid/gpstest.png
   :alt: GPSTest
   :target: ../galleries/f-droid/gpstest.png
   :class: center


Markor
------

Markor_ is a Markdown/text editor with many features. I mostly use it for simple to-do lists (todo.txt format) and unstructured notes.
I keep a ``temp.md`` to jot down things to remember (e.g. when talking to people).
All files are stored locally and can be synced with other apps (e.g. Syncthing).

.. image:: ../galleries/f-droid/markor-todo.png
   :alt: Markor todo.txt
   :target: ../galleries/f-droid/markor-todo.png
   :class: center

MGit
----

MGit_ is a generic git client. Works with any git provider (managed or self-hosted).
I use it sometimes to do last-minute edits (such as fixing typos) of blog posts.
The interface is slightly cumbersome as git primitives are directly exposed, i.e. you have to "pull", "edit", "add", "commit", and "push" to change a single file.

OsmAnd~
-------

I whole-heartedly recommend OsmAnd_ for your next travel needs. Its many features probably deserve a dedicated blog post, but here some of my uses:

I use the offline navigation feature when travelling. This usually works great both when driving or when on foot.

I use personal map markers (favorites) with custom categories and colors (accomodation = blue, attraction = red, restaurant = green, etc) to mark hotels and interesting places before and during trips.

.. image:: ../galleries/f-droid/osm-pois.png
   :alt: OsmAnd POIs
   :target: ../galleries/f-droid/osm-pois.png
   :class: center

I use the many different POI categories to find everything I need, e.g. gas stations, ATMs, sightseeing points.

I use OSM's advanced street attributes to know whether a street is actually a dirt road in bad condition (to avoid without 4x4) or a paved road.

OsmAnd can display Wikipedia entries as POIs and you can read the articles offline! This is pretty useful during travelling to learn some basic facts of your surroundings.

.. image:: ../galleries/f-droid/osm-wikipedia.png
   :alt: OsmAnd Wikipedia entries
   :target: ../galleries/f-droid/osm-wikipedia.png
   :class: center

You can edit and add POIs to upload to OpenStreetMap, e.g. I recently added and updated two hotels in Costa Rica (one was missing and the other changed its name).

Simple Clock
------------

`Simple Clock`_ is a clock! Yes, but it has a killer feature which the builtin Android clock does not have: you can configure Simple Clock to prevent display lock!
This feature is essential if you want a clock to be always-on during your conference talk to check timing.

Simple Gallery
--------------

`Simple Gallery`_ is a picture/video gallery with basic picture editing (e.g. crop and pen tool).

.. image:: ../galleries/f-droid/gallery.png
   :alt: Gallery: crop
   :target: ../galleries/f-droid/gallery.png
   :class: center

QuickDic
--------

QuickDic_ is a very useful offline dictionary which supports many languages.
Frequently used during my last vacation trip.

.. image:: ../galleries/f-droid/quickdic-dicts.png
   :alt: QuickDic
   :target: ../galleries/f-droid/quickdic-dicts.png
   :class: center

.. image:: ../galleries/f-droid/quickdic.png
   :alt: QuickDic
   :target: ../galleries/f-droid/quickdic.png
   :class: center

StreetComplete
--------------

The app StreetComplete_ shows "tasks" to complete OpenStreetMap data, e.g. to add building height, road surface, and store opening hours.
I use the app when waiting for public transportation to help improve OpenStreetMap information. The app is easy to use: tasks are simple multiple-choice questions.

Tusky
-----

Tusky_ is a Mastodon client for Android.

Summary
-------

There are some pretty cool F-Droid apps I frequently recommend to people (e.g. OsmAnd_ and Markor_), but this list is not very extensive and I still use a lot of non-free apps (Google stuff, Twitter, ..).
Discovering good F-Droid apps is currently not very easy (there are no stars or app reviews), so I would be glad to hear your recommendations! Ping me `on Twitter <https://twitter.com/try_except_>`_ or `Mastodon <https://floss.social/@hjacobs>`_.


.. _F-Droid: https://f-droid.org/
.. _2048: https://f-droid.org/en/packages/org.secuso.privacyfriendly2048/
.. _Amaze: https://f-droid.org/en/packages/com.amaze.filemanager/
.. _Delta Chat: https://f-droid.org/en/packages/com.b44t.messenger/
.. _DNS66: https://f-droid.org/en/packages/org.jak_linux.dns66/
.. _GitNex: https://f-droid.org/en/packages/org.mian.gitnex/
.. _GPSTest: https://f-droid.org/en/packages/com.android.gpstest.osmdroid/
.. _Markor: https://f-droid.org/en/packages/net.gsantner.markor/
.. _MGit: https://f-droid.org/en/packages/com.manichord.mgit/
.. _OsmAnd: https://f-droid.org/en/packages/net.osmand.plus/
.. _Simple Clock: https://f-droid.org/en/packages/com.simplemobiletools.clock/
.. _Simple Gallery: https://f-droid.org/en/packages/com.simplemobiletools.gallery.pro/
.. _QuickDic: https://f-droid.org/en/packages/de.reimardoeffinger.quickdic/
.. _StreetComplete: https://f-droid.org/en/packages/de.westnordost.streetcomplete/
.. _Tusky: https://f-droid.org/en/packages/com.keylesspalace.tusky/
