.. title: Naming Applications and Microservices
.. slug: naming-applications-components-microservices
.. date: 2020/07/04 18:18:00
.. tags:
.. link:
.. description: Going bananas with naming microservices? This post proposes a guidelines to follow when naming internal applications and components.
.. previewimage: ../galleries/naming-applications-components/magic-baby.jpg
.. type: text

.. image:: ../galleries/naming-applications-components/magic-baby.jpg
   :class: left

Naming is hard.
Naming is communication.
Going bananas with naming microservices?
Probably something you will regret!
This post proposes a few guidelines to follow when naming internal applications and components.

.. TEASER_END

Developers seem to enjoy using clever, cute, funny names for their applications [#]_.
I see many tech companies using codenames for internal applications and microservices.
For example, Airbnb goes bananas with "Banana":

.. image:: ../galleries/naming-applications-components/twitter-airbnb-banana.png
   :class: center
   :alt: Years ago there started a trend at @AirbnbEng to come up with clever cheeky names for services. Now it's gotten out of control. Exhibit A: the internal web app for viewing microservice docs is called "Banana". 🍌  What does it mean??
   :target: https://twitter.com/spikebrehm/status/1100566261191258114

Twitter engineers seem to like bird names (no surprise):

.. image:: ../galleries/naming-applications-components/twitter-twitter-names.png
   :class: center
   :alt: We did this at Twitter. It got to the point where I had no idea what people were talking about anymore. Hummingbird, dodo, snowflake, Parrott, we're all real system names
   :target: https://twitter.com/mjackson/status/1100624311872679938

Spotify seems to have a mixture of codenames ("Bieber", "horus", "hotdog") and meaningful system names ("charts-api", "userdata").
Slide from a `talk given by the CTO in 2016 <https://www.slideshare.net/kevingoldsmith/how-spotify-builds-products-organization-architecture-autonomy-accountability/112>`_:

.. image:: ../galleries/naming-applications-components/spotify-system-names.jpg
   :class: center
   :alt: Spotify system names
   :target: ../galleries/naming-applications-components/spotify-system-names.jpg

The Rockefeller Archive Center `uses constellation-themed names for microservices <https://blog.rockarch.org/project-electron-update-building-microservices-for-integration>`_ ("aquarius", "fornax", "gemini"):

.. image:: ../galleries/naming-applications-components/rockarch-microservices-overhead.png
   :class: center
   :alt: Rockefeller Archive Center microservices
   :target: ../galleries/naming-applications-components/rockarch-microservices-overhead.png

Zalando also started to use "fun" team and application IDs when introducing microservices in 2015.
Teams started to have naming schemes for their applications, e.g. "coastal shapes" or "characters from Agatha Christie's Poirot".
Some Georgian names turned out to be successful Open Source projects:


.. image:: ../galleries/naming-applications-components/twitter-zalando-georgian-names.png
   :class: center
   :alt: Twitter: Our Georgian heritage at @ZalandoTech
   :target: https://twitter.com/try_except_/status/1266108151646035969

The `orange site has more suggestions for ridiculous names <https://news.ycombinator.com/item?id=1942384>`_
and `namingschemes.com <https://namingschemes.com/>`_ can be your inspiration, too.
So if you want to go crazy with naming, by all means do, but please consider the consequences.
`Naming your microservices needs to be trivial, not cryptic <https://theiconic.tech/naming-your-microservices-needs-to-be-trivial-not-cryptic-288902e800e1>`_:

    How does this sound? I deployed “Leonardo” and it broke “Ernest”

VS

    I deployed “shipments” and that caused the outage on “pick and pack” too.

Or do you want your meetings to `sound like this <https://www.youtube.com/watch?v=y8OnoxKotPQ>`_?

Guidelines
==========

If random "fun" names are not the way to go, how to name internal applications, components, and microservices?
Here some proposed guidelines.

Application names…

* … MUST NOT contain offensive terms
* … MUST NOT include organization/team identifiers
* … SHOULD describe the application's purpose
* … SHOULD NOT consist solely of acronyms
* … SHOULD NOT use broad/general names for local purposes
* … SHOULD NOT be clever or rely on temporary concepts
* … CAN represent a product branding (if established)

Names MUST NOT contain offensive terms
--------------------------------------

Avoid offensive words like "blacklist", "master", and "slave". Avoid violent names like "kill", "killer", and "war".
Also avoid political terms like "brexit" and "putin". Avoid names which can sound offensive for your colleagues,
e.g. "Den Den Mushi" are `fictional snails <https://onepiece.fandom.com/wiki/Den_Den_Mushi>`_, but "Mushi" sounds a lot like "Muschi" (pussy) in German.
Other terminology can also be problematic. For example, imagine you work in a telecommunication company and name one of your services "big brother"
— guess what happens if this name is leaked to the press?

Names MUST NOT include organization/team identifiers
----------------------------------------------------

Applications typically outlive their organization structure.
Adding department/team identifiers to your application names will cause confusion and unnecessary renaming.
This is especially problematic if your org/team names are codenames.
For example, team "kingpins" has created an application "kingpins-user-frontend", what happens if the application is handed over to team "gravity"?

Microservices should be owned by exactly one engineering team, but they should not reflect the team's identity.

Names SHOULD describe the application's purpose
-----------------------------------------------

The application's name should describe its business purpose, i.e. why it exists in the first place.
The application's purpose should not change, otherwise we need to introduce a new application with a different name.
A microservice should have one responsibility. This should be reflected in its name.

Don't assume that others will have the same context as you and understand the name's meaning.
Make sure to ask colleagues who are not familiar with your business domain to review your names.

Names SHOULD NOT consist solely of acronyms
-------------------------------------------

Acronyms are sometimes a necessary evil, but avoid acronyms unless they are well established in your company.
"CMS" (Content Management System) is a well established acronym. CLOTS, BUGR, and NIPL are probably not well-known.
Avoid cryptic acronyms.

Names SHOULD NOT use broad/general names for local purposes
-----------------------------------------------------------

Large organizations tend to use similar entities in different domains, e.g. there is probably more than one "item", "order", and "user" entity in your company.
Avoid using too general names like "item processor" or "user service" unless you are absolutely sure that your organization only has exactly one definition for "item" and "user".

Names SHOULD NOT be clever or rely on temporary concepts
--------------------------------------------------------

I wholeheartedly agree with `Austerity as a naming principle <https://www.namingthings.co/naming-things-principles>`_:

    It can be tempting to use a bit of humor or a cultural reference in code to lighten the mood a bit, but doing this can violate many principles. For example, a joke name can reduce understandability of the underlying concept and reduce the name’s consistency and searchability. Colloquialisms (which are only understandable to certain audiences) and puns can cause similar issues. They may seem fun initially, but after dealing with them for years, that fun will turn into apathy and (more likely!) antipathy. When in doubt, choose names that are literal and dry.

Names CAN represent a product branding
--------------------------------------

Internal application names should not be cryptic codenames, but establishing a brand sometimes makes sense.
Don't introduce a codename unless you are willing to invest into establishing a (product) brand, e.g. a well-recognized codename within the company or for promoting an Open Source project.
Spotify released their developer portal "Backstage" which is now a `recognized OSS project <https://github.com/spotify/backstage>`_.
Zalando has well-established codenames for central infrastructure components like Nakadi_ and Skipper_.
Almost every Zalando engineer knows what Nakadi and Skipper are.

Your local "raccoon" and "wingman" microservices don't need branding, their codenames `just lead to confusion <https://www.youtube.com/watch?v=y8OnoxKotPQ>`_.

Summary
=======

Naming is hard, but there is no shortcut for finding a good name.
Selecting or inventing a naming scheme
and assigning a random name to each application sounds like fun.
Please don't prioritize this fun over the struggle of all your colleagues having to deal with these "fun" names.
Involve colleagues from other departments to find a "good" name which expresses the application's purpose.

Stop proliferating meaningless "fun" codenames for software systems.

Further Reading
===============

* `The 7 principles of naming <https://www.namingthings.co/naming-things-principles>`_
* `Friends don't allow Friends to create Microservices with Codenames <http://www.russmiles.com/essais/friends-dont-allow-friends-to-create-microservices-with-codenames>`_
* `Naming your microservices needs to be trivial, not cryptic <https://theiconic.tech/naming-your-microservices-needs-to-be-trivial-not-cryptic-288902e800e1>`_
* `Creating inclusive naming conventions in technology <https://www.clockwork.com/news/creating-inclusive-naming-conventions-in-technology/>`_


.. image:: ../galleries/naming-applications-components/microservices.jpg
   :class: center
   :alt: Microservices on YouTube
   :target: https://www.youtube.com/watch?v=y8OnoxKotPQ

.. [#] I will use the term "application" in this blog post, but you can replace it with "component", "system", "service", or "microservice", depending on what entities you use in your organization. My definition for application is "any software system fulfilling some business purpose".


.. _Nakadi: https://nakadi.io/
.. _Skipper: https://github.com/zalando/skipper
