.. title: Laser Cutting
.. slug: laser-cutting
.. date: 2021/09/11 15:15:00
.. tags:
.. link:
.. description: Laser cutting plywood and acrylic glass with Mr Beam
.. previewimage: ../galleries/laser-cutting/laser-warning.jpeg
.. type: text

.. image:: ../galleries/laser-cutting/laser-warning.jpeg
   :class: left

Laser cutting with plywood, acrylic glass, or paper can be fun! In this post, I'll share which laser cutter I bought last year, what I created so far,
and what I learned along the way.

.. TEASER_END

While 3D printers look interesting, working with wood (instead of plastic) appeals more to me.
So I decided to buy a laser cutter instead of a 3D printer last year.
I went for the `Mr Beam II Dreamcut <https://www.mr-beam.org/>`_. It's a bit pricey, but I wanted something without any hassle and without needing some cloud connection (Mr Beam provides an offline web UI in the local WiFi).
Delivery and unpacking was very smooth. I also ordered a pack of 3mm plywood to get started.
Here the Mr Beam in action making some plywood box walls:

.. image:: ../galleries/laser-cutting/laser-cutting-in-progress2.jpeg
   :alt: Mr Beam II Dreamcut in action
   :target: ../galleries/laser-cutting/laser-cutting-in-progress2.jpeg
   :class: center

Laser cutting produces a lot of bad fumes from the burned material. Plywood smoke is no fun to inhale.
Mr Beam offers an air filter system for €690 to operate indoors.
I decided to not buy the Mr Beam air filter, but instead use the provided tube and build an adapter to push the exhaust out through a metal "window".
So my first laser cut project was the air outlet adapter.

First I generated a box in the right size via the `boxes.py website <https://festi.info/boxes.py/>`_ (online SVG generator for laser cutting).
Then I designed the rest of the air outlet in `Inkscape <https://inkscape.org/>`_ on Linux:

.. image:: ../galleries/laser-cutting/air-outlet.png
   :alt: Screenshot of editing air outlet SVG in Inkscape
   :target: ../galleries/laser-cutting/air-outlet.png
   :class: center

This went pretty smooth considering it was my first laser cut project. The finished air outlet looks like this:

.. image:: ../galleries/laser-cutting/air-outlet.jpeg
   :alt: Finished air outlet
   :target: ../galleries/laser-cutting/air-outlet.jpeg
   :class: center

Materials
=========

What materials can you cut? Even though the Mr Beam laser cutter only has a 5W laser, it `can cut various materials <https://www.mr-beam.org/en/materials/>`_.
Here I tried a few materials (from left to right): 0.8mm polypropylene, 1mm acrylic glass (Plexiglass), 2mm solid wood, and 3mm black acrylic glass:

.. image:: ../galleries/laser-cutting/star-different-materials.jpeg
   :alt: Star shape cut from different materials
   :target: ../galleries/laser-cutting/star-different-materials.jpeg
   :class: center

I mostly cut 3mm and 4mm plywood (poplar) which works great, but sometimes the plywood quality is very bad (e.g. uneven thickness, knotholes) and it burns out:

.. image:: ../galleries/laser-cutting/uneven-plywood.jpeg
   :alt: Uneven thickness of 4mm plywood
   :target: ../galleries/laser-cutting/uneven-plywood.jpeg
   :class: center

.. image:: ../galleries/laser-cutting/burnt-wood.jpeg
   :alt: Burnt plywood with knothole
   :target: ../galleries/laser-cutting/burnt-wood.jpeg
   :class: center

So far I successfully have cut:

* standard printer paper: fast to cut, sometimes needs extra 100ms pierce time
* 0.8 mm polypropylene (grey and black): melts a bit at the edges (result does not look perfect)
* 3mm and 4mm plywood (poplar): my default material
* 1mm and 3mm acrylic glass (red and black): yields nice results
* 3mm acrylic felt: easy and fast to cut
* colored (thick) paper: works great, but leaves some dark dust (from the color ink?)
* rubber foam (Moosgummi): really easy and very fast to cut

Useful Constructions
====================

I'm always looking for useful constructions I can make with the laser cutter.
Here a selection of different things I made so far.

My desk needed a place for my headphones, so I created a headphone holder from 3 layers of plywood:

.. image:: ../galleries/laser-cutting/headphone-holder.jpeg
   :alt: Headphone holder attached to desk
   :target: ../galleries/laser-cutting/headphone-holder.jpeg
   :class: center

Plywood made from poplar is very soft. It's not the perfect material to lift heavy objects,
but it turns out that it's surprisingly sturdy when constructing the right boxes.
I needed a monitor stand for my desk which allows to store my keyboard below it.
I designed two boxes with drawers and cable channels. A solid wood board connects the two boxes and has the monitor on top.
Here the monitor stand from 4mm plywood with small drawers and cable channels:

.. image:: ../galleries/laser-cutting/monitor-stand1.jpeg
   :alt: Monitor stand made from plywood
   :target: ../galleries/laser-cutting/monitor-stand1.jpeg
   :class: center

The drawers contain 3mm grey acrylic felt as soft inlay and use old shoe laces as drawer handles:

.. image:: ../galleries/laser-cutting/monitor-stand2.jpeg
   :alt: Monitor stand with open drawer
   :target: ../galleries/laser-cutting/monitor-stand2.jpeg
   :class: center


I built a `food house for birds <https://codeberg.org/hjacobs/laser-cut-templates/src/branch/main/bird-food-box>`_ (and squirrels) out of 4mm plywood. Here a squirrel enjoying peanuts I put into the house every day:

.. image:: ../galleries/laser-cutting/bird-food-house-squirrel.jpeg
   :alt: Finished food house with squirrel visitor
   :target: ../galleries/laser-cutting/bird-food-house-squirrel.jpeg
   :class: center

Here a great tit visiting the food house (note that the roof changed slightly as I replaced the strip of plastic with plywood):

.. image:: ../galleries/laser-cutting/bird-food-house-great-tit.jpeg
   :alt: Finished food house with great tit as visitor
   :target: ../galleries/laser-cutting/bird-food-house-great-tit.jpeg
   :class: center

Using 2 layers of premium paint (Sikkens Cetol Filter 7 Plus) made the plywood weather resistant. So far the wooden house survived multiple rain storms.

Other constructions I created: a cat food tray, various small shelves, drawer separators, and a tray integrated with LED lamp.

Just for Fun
============

Some things I just made for fun or to experiment. For example, I played with an algorithm to construct a meander / labyrinth (`Anni' Albers Meander Python generator <https://codeberg.org/hjacobs/anni-albers-meander>`_):

.. image:: ../galleries/laser-cutting/meander1.jpeg
   :alt: Meander cut from plywood
   :target: ../galleries/laser-cutting/meander1.jpeg
   :class: center

.. image:: ../galleries/laser-cutting/meander2.jpeg
   :alt: The two meander pieces separated
   :target: ../galleries/laser-cutting/meander2.jpeg
   :class: center

I experimented a lot with `visual cryptography <https://codeberg.org/hjacobs/laser-cut-templates/src/branch/main/visual-cryptography>`_, i.e. cutting two cards with random holes which reveal a secret when put together.
The trick is that each card is completely random and contains no information, but put together they contain information.
Here two cards with completely random holes:

.. image:: ../galleries/laser-cutting/visual-cryptography1.jpeg
   :alt: Two visual cryptography cards made from acrylic glass
   :target: ../galleries/laser-cutting/visual-cryptography1.jpeg
   :class: center

Put together they reveal glyphs ("Y" and "U" marked red as example):

.. image:: ../galleries/laser-cutting/visual-cryptography2.jpeg
   :alt: Two visual cryptography cards revealing the letters "Y" and "U"
   :target: ../galleries/laser-cutting/visual-cryptography2.jpeg
   :class: center

Another example with larger glyphs and red cardboard (do you find the "5", "9", and "L"?):

.. image:: ../galleries/laser-cutting/visual-cryptography3.jpeg
   :alt: Two visual cryptography cards in red revealing the characters "5", "9", and "L"
   :target: ../galleries/laser-cutting/visual-cryptography3.jpeg
   :class: center

While visual cryptography was more of an experiment with limited value,
the napkin ring actually works as a "design" object:

.. image:: ../galleries/laser-cutting/napkin-ring1.jpeg
   :alt: Napkin ring made from plywood
   :target: ../galleries/laser-cutting/napkin-ring1.jpeg
   :class: center

It's made from three layers of 4mm plywood:

.. image:: ../galleries/laser-cutting/napkin-ring2.jpeg
   :alt: Napkin ring made from plywood
   :target: ../galleries/laser-cutting/napkin-ring2.jpeg
   :class: center

I did not paint the napkin ring as I think the natural (burnt) wood looks good in this case.

More recently I discovered that making rubber stamps from foam rubber ("Moosgummi") is really easy to do.
After creating my own logo stamp, I now mark all my books with my personal logo:

.. image:: ../galleries/laser-cutting/rubber-stamp-logo.jpeg
   :alt: Book stamped with personal logo
   :target: ../galleries/laser-cutting/rubber-stamp-logo.jpeg
   :class: center

Wrapping Up
===========

So far I have no regrets buying the laser cutter.
I enjoy building little boxes from plywood for various purposes.
I usually start with boxes.py and then modify the generated SVG to my needs.
Painting them with an oil/wax combination (Osmo Dekorwachs, mahogany color) makes them look more valuable than they really are.
As you can see, the next box is already waiting drying for its second layer of paint :-)

.. image:: ../galleries/laser-cutting/another-box-drying.jpeg
   :alt: Plywood box painted and drying
   :target: ../galleries/laser-cutting/another-box-drying.jpeg
   :class: center

Links
=====

* `Mr Beam online store <https://www.mr-beam.org/>`_
* `boxes.py online generator <https://festi.info/boxes.py/>`_
* `Anni Albers's Meander generator (Python script) <https://codeberg.org/hjacobs/anni-albers-meander>`_
* `Visual Cryptography generators (experimental Python scripts) <https://codeberg.org/hjacobs/laser-cut-templates/src/branch/main/visual-cryptography>`_
