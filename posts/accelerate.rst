.. title: Accelerate: Software Delivery Performance
.. slug: accelerate-software-delivery-performance
.. date: 2019/04/16 21:33:00
.. tags:
.. link:
.. description:
.. previewimage: ../galleries/accelerate.png
.. type: text

.. image:: ../galleries/accelerate.png
   :class: left
   :alt: Accelerate book

The `Accelerate book <https://itrevolution.com/book/accelerate/>`_ is about "measuring software delivery performance — and what drives it — using four years of groundbreaking research and rigorous statistical methods".
I'll try to explain why I see the book as a blueprint for platform teams and how it relates to developer experience.

.. TEASER_END

My current role in Zalando_ ("Head of Developer Productivity") involves taking care of several platform teams which enable 200+ delivery teams to build, run, and operate their applications on `Kubernetes/AWS <https://kubernetes-on-aws.readthedocs.io/en/latest/admin-guide/public-presentations.html>`_.
So my role *is* driving software delivery performance and developer experience — what better book than *Accelerate* could there be for me?

I start by quoting our former VP Engineering `Eric Bowman <https://twitter.com/ebowman>`_:

   "*Accelerate* .. The idea is simple: broad primary research shows that there is a causal relationship between three KPIs related to continuous delivery, and business performance and great culture. This is actually completely and utterly profound. Causal. We should lean hard on this (and we are)."

Software Delivery Metrics
-------------------------

We all can agree that `measuring productivity is hard <https://www.martinfowler.com/bliki/CannotMeasureProductivity.html>`_, so what metrics should we look at for software delivery performance?

The book shows that these four metrics are worth tracking and optimizing:

* **Deployment Frequency**: how often do we deploy?
* **Lead Time**: how long does it take from code committed to code successfully running in production?
* **Mean Time to Restore (MTTR)**: how quickly can a service be restored?
* **Change Fail Rate**: what percentage of changes to production fail?

Deployment Frequency and Lead Time are both measuring software delivery performance *tempo*. Mean Time to Restore and Change Fail Rate are measures for reliability.

*Accelerate*'s research collected over 23,000 survey responses from over 2,000 unique organizations, from startup to large enterprise.
One major finding (not surprising for most, but maybe for some):

    "Astonishingly, these results demonstrate that there is **no trade-off between improving performance and achieving higher levels of stability and quality**."

So high performance IT organizations go both faster *and* safer!

Why do these software delivery performance metrics matter?

    "Software delivery performance predicts organizational performance and noncommercial performance."

Optimizing software delivery performance is worth doing if you want to build a high performing technology organization!

Optimizing
----------

Why do I see the *Accelerate* book as a blueprint for platform teams catering for internal developers?
The book shows how good practices like Lean Management, Continuous Delivery, Automation, etc affect software delivery performance which in turn affects organizational performance [#]_.
Medium to large companies benefit from central platform teams driving these good practices across the organization — economy of scale instead of local optimizations.
If anybody asks whether your internal CI/CD tooling team is worth investing in, give the person a copy of *Accelerate* ;-)

.. image:: ../galleries/meme-lord-saviour-accelerate.png
   :class: center
   :alt: Meme: Excuse me, Sir, Do you have a moment to talk about our lord and saviour "Accelerate"?

The book lists a number of well-known techniques to optimize software delivery performance (*Accelerate* calls them "key capabilities"):

* Continuous Delivery

  * Use version control for all production artifacts
  * Automate your deployment process
  * Implement continuous integration
  * Use trunk-based development methods [#]_
  * Implement test automation
  * Support test data management
  * Shift left on security
  * Implement continuous delivery (CD)

* Architecture

  * Use a loosely coupled architecture
  * Architect for empowered teams

* Product and Process

  * Gather and implement customer feedback
  * Make the flow of work visible through the value stream
  * Work in small batches
  * Foster and enable team experimentation

* Lean Management and Monitoring

  * Have a lightweight change approval process
  * Monitor across application and infrastructure to inform business decisions
  * Check system health proactively
  * Improve processes and manage work with work-in-progress (WIP) limits
  * Visualize work to monitor quality and communicate throughout the team

* Culture

  * Support a generative culture (as `outlined by Westrum <https://www.andykelk.net/devops/using-the-westrum-typology-to-measure-culture>`_)
  * Encourage and support learning
  * Support and facilitate collaboration among teams
  * Provide resources and tools that make work meaningful
  * Support or embody transformational leadership

While some of the above (e.g. version control, deployment automation) are taken for granted, others definitely need investment from our side as platform teams in Zalando.

Developer Experience
--------------------

What does software delivery performance have to do with developer experience? *Accelerate*'s findings are not surprising at all:

* High performing technology organizations have **better employee loyalty** (eNPS)
* Employees in high-performing teams are 1.8 times more likely to **recommend their team** as a great place to work
* **Job satisfaction** predicts organizational performance
* Evidence shows that technical and Lean management practices contributed to **reductions in both burnout and deployment pain**

In short: Lean Management and Continuous Delivery practices improve culture, developer experience, and job satisfaction.

In 2017, we started to measure developer experience in Zalando quarterly with a simple question: "Overall, how satisfied or dissatisfied are you with your Zalando developer experience?" (1-10 scale from "Extremely Dissatisfied" to "Extremely Satisfied"):

.. image:: ../galleries/zalando-developer-survey.png
   :class: center
   :alt: Zalando Developer Survey form

I see *Accelerate* as a strategic blueprint for us to optimize software delivery performance and further strengthen the developer experience at Zalando. We use the *Accelerate* metrics as KPIs/OKRs and started investigating how to leverage the practices effectively for our 1200+ customers (Zalando developers). If you want to know more about our approach regarding developer experience, check out my past `presentation about Developer Experience at Zalando <https://www.slideshare.net/try_except_/developer-experience-at-zalando-handelsblatt-strategisches-itmanagement-2019>`_.

What are your thoughts on *Accelerate*? How do you measure and optimize software delivery performance in your organization?
How do you drive developer experience as an internal platform team?
**I want to hear from you!** Please `contact me on Twitter <https://twitter.com/try_except_>`_.

.. [#] To get a complete overview of the research, practices, and how things are interconnected, see `Accelerate's Overall Research Program diagram as PDF <https://devops-research.com/assets/transformation_practices.pdf>`_.
.. [#] There are constant fights about `trunk based development <https://trunkbaseddevelopment.com/>`_, so the book makes clear that short-lived branches that are merged into trunk ("master") at least daily are fine, too: "We .. found that teams using branches that live a short amount of time (integration times less than a day) combined with short merging and integration periods (less than a day) do better in terms of software delivery performance than teams using longer-lived branches"

.. _Zalando: https://tech.zalando.com/
