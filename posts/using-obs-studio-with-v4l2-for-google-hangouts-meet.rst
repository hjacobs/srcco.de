.. title: Using OBS Studio with V4L2 for Google Meet
.. slug: using-obs-studio-with-v4l2-for-google-hangouts-meet
.. date: 2020/03/16 16:21:00
.. tags: xubuntu
.. link:
.. description:
.. previewimage: ../galleries/obs-studio-v4l2/obs-studio.png
.. type: text

.. image:: ../galleries/obs-studio-v4l2/obs-studio.png
   :class: left

Many people are now working remote ("WFH").
How to make the best out of video calls with Google Hangouts Meet?
This blog post describes how to use `OBS Studio <https://obsproject.com/>`_ with a virtual camera device (video loopback)
on Linux to add effects and scenes for Google Meet video calls.

.. TEASER_END

DISCLAIMER: I use Xubuntu 19.10 and have not tested the instructions on any other Linux installation.

Setting up OBS Studio
---------------------

OBS Studio is free and open source software for video recording and live streaming.
Let's install its package on Ubuntu:

.. code-block:: bash

   sudo apt install obs-studio

Start OBS Studio and configure your scenes. The default scene should obviously include your camera image.
You can include arbitrary image effects. Here some added background image, video mask, and color effect:

.. image:: ../galleries/obs-studio-v4l2/obs-studio-video-scene.png
   :class: center
   :target: ../galleries/obs-studio-v4l2/obs-studio-video-scene.png

I prepared multiple scenes to signal different messages to my colleagues without unmuting myself, e.g.
"Thumbs up / +1", "AFK", or "Thank You!":

.. image:: ../galleries/obs-studio-v4l2/obs-studio-thank-you.png
   :class: center
   :target: ../galleries/obs-studio-v4l2/obs-studio-thank-you.png

Configuring the Virtual Camera Device
--------------------------------------

Google Meet (in Firefox) can use a "virtual camera" Linux video device.
You need to configure a Video4Linux (V4L) loopback device which OBS Studio can write to.

Install the V4L2 loopback driver:

.. code-block:: bash

   sudo apt install v4l2loopback-dkms

We need to set up the virtual camera device. I use ``/dev/video10`` to not collide with any existing devices:

.. code-block:: bash

   sudo modprobe v4l2loopback devices=1 video_nr=10 card_label="OBS Cam" exclusive_caps=1

After experimenting with ffmpeg and high latencies, I swiched to `this OBS plugin <https://github.com/CatxFish/obs-v4l2sink>`_ to directly write to the v4l2 video device.
It needs some compilation, so let's compile and install the plugin:

.. code-block:: bash

   sudo apt install qtbase5-dev
   git clone --recursive https://github.com/obsproject/obs-studio.git
   git clone https://github.com/CatxFish/obs-v4l2sink.git
   cd obs-v4l2sink
   mkdir build && cd build
   cmake -DLIBOBS_INCLUDE_DIR="../../obs-studio/libobs" -DCMAKE_INSTALL_PREFIX=/usr ..
   make -j4
   sudo make install


Now start OBS Studio and configure the V4L2 video output to ``/dev/video10`` via the new menu item "Tools"->"V4L2 Video Output":

.. image:: ../galleries/obs-studio-v4l2/obs-v4l2-output-dialog.png
   :class: center

Using Google Meet with the Virtual Camera
-----------------------------------------

Start a Google Meet via https://meet.google.com/ and go to settings:

.. image:: ../galleries/obs-studio-v4l2/google-meet-settings.png
   :class: center

Configure the video camera to use the video loopback device "OBS Cam":

.. image:: ../galleries/obs-studio-v4l2/google-meet-settings-video.png
   :class: center

Join the video call and enjoy your OBS-generated video image:

.. image:: ../galleries/obs-studio-v4l2/google-meet-video-call.png
   :class: center

NOTE: Google Meet will show your video stream horizontally flipped, but only for you!
Others will see the "Thank You!" text correctly.

UPDATE 2020-03-24
-----------------

Somebody told me that it also works on Ubuntu 18.04 LTS ("bionic"), but needs the installation of ``libobs-dev`` (``apt install libobs-dev``)
and the OBS plugin needs to be copied to the right place:

.. code-block:: bash

   cp /usr/lib/obs-plugins/v4l2sink.so /usr/lib/x86_64-linux-gnu/obs-plugins/

I did not test this myself, but I hope it helps LTS users :-)
