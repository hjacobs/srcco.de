.. title: Kubernetes on AMD64 with PC Engines APU2
.. slug: pc-engines-apu2-ubuntu-18.04-kubernetes-amd64-server
.. date: 2019/07/13 10:40:00
.. tags: apu, kubernetes
.. link:
.. description:
.. previewimage: ../galleries/apu2/apu2-package-4.jpg
.. type: text

.. image:: ../galleries/apu2/apu2-package-4.jpg
   :class: left

I want to run Kubernetes on amd64 hardware. I already own two Raspberry PIs, but I always struggle with ARM and the need to create custom Docker images.
This post explains how to run a single-node Kubernetes (K3s_) on the `PC Engines APU2 board`_ with an embedded AMD CPU.

.. TEASER_END

I bought the `APU2D0 board variant with 2GB RAM`_ (101.75 €), red case (10.10 €), 60GB mSATA SSD (39.09 €), and power adapter (5.29 €) from VARIA_.
I also ordered the `USB serial adapter`_ (extra 10.49 €) as the APU does not have any video output and I don't have any serial port.
The total order amount was 179.08 € incl. 19% VAT and shipping costs.

.. _APU2D0 board variant with 2GB RAM: https://www.varia-store.com/en/produkt/34339-pc-engines-apudc0-system-board-2x-lan-2-gb-ram.html
.. _USB serial adapter: https://www.varia-store.com/en/produkt/13456-usb-to-db9f-serial-adapter-incl-1-5-m-usb-to-microusb-cable.html

Unpacking
=========

The order was delivered within 3 days and the APU came in a nice box:

.. image:: ../galleries/apu2/apu2-package-1.jpg
   :alt: PC Engines APU2 box
   :target: ../galleries/apu2/apu2-package-1.jpg
   :class: center

.. image:: ../galleries/apu2/apu2-package-2.jpg
   :alt: PC Engines APU2 box
   :target: ../galleries/apu2/apu2-package-2.jpg
   :class: center

.. image:: ../galleries/apu2/apu2-package-3.jpg
   :alt: PC Engines APU2 box
   :target: ../galleries/apu2/apu2-package-3.jpg
   :class: center

After unpacking, everything was there incl. heat spreader kit and screws for the enclosure. Note that the APU does not come with any manual.

.. image:: ../galleries/apu2/apu2-package-4.jpg
   :alt: PC Engines APU2 box contents
   :target: ../galleries/apu2/apu2-package-4.jpg
   :class: center

I first installed the SSD into the mSATA slot:

.. image:: ../galleries/apu2/apu2-package-5.jpg
   :alt: PC Engines APU2 with SSD installed
   :target: ../galleries/apu2/apu2-package-5.jpg
   :class: center

The AMD GX-412TC CPU is passively cooled by heat conduction to the bottom of the enclosure.
`Assembling the heat spreader kit <https://pcengines.ch/apucool.htm>`_ was a bit tricky (putting the small thermal pad and heat spreader at the right location), but easy enough.
After screwing the board to the enclosure and connecting everything, the board boots up (photo shows USB stick already connected):

.. image:: ../galleries/apu2/apu2-connected.jpg
   :alt: PC Engines APU2 connected
   :target: ../galleries/apu2/apu2-connected.jpg
   :class: center

Installing Ubuntu
=================

I use Ubuntu 18.04 LTS as the server's operating system. So let's download ``mini.iso`` from `the netboot installer dir <http://archive.ubuntu.com/ubuntu/dists/bionic-updates/main/installer-amd64/current/images/netboot/>`_ and write it to a USB stick:

.. code-block:: bash

    $ dd if=~/Downloads/mini.iso of=/dev/sdXX bs=1M  # /dev/sdXX must be the USB stick

The APU does not have any video output, so we need to `use the serial console`_, e.g. via PuTTY:

.. code-block:: bash

    $ sudo adduser hjacobs dialout
    $ # logout & login
    $ sudo apt install putty

As I use the ordered USB serial adapter, the serial port is ``/dev/ttyUSB0``.

.. image:: ../galleries/apu2/putty.png
   :alt: PuTTY configuration dialog
   :class: center

Connect with PuTTY and boot up the APU with the USB stick connected. Pressing F10 during boot allows us to start the Ubuntu installation from USB stick:

.. image:: ../galleries/apu2/coreboot-boot-menu.png
   :alt: Coreboot boot menu
   :target: ../galleries/apu2/coreboot-boot-menu.png
   :class: center

Select "Install" and press <TAB> to edit the command line to ``linux initrd=initrd.gz console=ttyS0,115200``:

.. image:: ../galleries/apu2/ubuntu-install-menu.png
   :alt: Ubuntu install menu
   :target: ../galleries/apu2/ubuntu-install-menu.png
   :class: center

Install Ubuntu 18.04 LTS to your liking. Also directly install the OpenSSH Server.

After a reboot and login via SSH, modify GRUB to start with serial console:

.. code-block:: bash

    $ sudo vi /etc/default/grub
    GRUB_CMDLINE_LINUX="console=ttyS0,115200n8"

    $ sudo update-grub
    $ sudo reboot

The next boot should automatically bring up the default Ubuntu login via serial console (PuTTY):

.. image:: ../galleries/apu2/putty-apu-login.png
   :alt: PuTTY Ubuntu Login
   :target: ../galleries/apu2/putty-apu-login.png
   :class: center

We can now use either SSH or the serial console for further steps.

Let's also configure temperature sensors:

.. code-block:: bash

    $ sudo apt install lm-sensors
    $ sudo sensors-detect
    $ sensors

Installing Kubernetes (K3s)
===========================

K3s_ does not require Docker, so we can install K3s directly:

.. code-block:: bash

    $ curl -sfL https://get.k3s.io | sh -

Check that it's running:

.. code-block:: bash

    $ sudo kubectl get pod --all-namespaces
    NAMESPACE     NAME                         READY   STATUS      RESTARTS   AGE
    kube-system   coredns-695688789-5dspn      1/1     Running     0          76s
    kube-system   helm-install-traefik-rsrbm   0/1     Completed   0          76s
    kube-system   svclb-traefik-fxg8d          2/2     Running     0          18s
    kube-system   traefik-56688c4464-wbjrt     0/1     Running     0          18s

K3s does not ship with the Kubernetes Metrics API for CPU/memory metrics. Let's install the Metrics Server:

.. code-block:: bash

    $ git clone git@github.com:kubernetes-incubator/metrics-server.git
    $ sudo kubectl apply -f metrics-server/deploy/1.8+/
    $ sudo kubectl top node
    NAME   CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%
    apu    188m         4%     551Mi           29%

Finally install `Kubernetes Operational View`_ to have some minimal Kubernetes web UI:

.. code-block:: bash

    $ git clone https://github.com/hjacobs/kube-ops-view.git
    $ sudo kubectl apply -f kube-ops-view/deploy/
    $ sudo kubectl edit ingress kube-ops-view
    # change the hostname to kube-ops-view.apu.local

K3s ships with Traefik as Ingress controller, so we can point ``kube-ops-view.apu.local`` to the local IP of the APU server and get the Kubernetes Operational View in the browser:

.. image:: ../galleries/apu2/kube-ops-view.png
   :alt: kube-ops-view in Firefox
   :target: ../galleries/apu2/kube-ops-view.png
   :class: center

Screwing together the top enclosure leaves us with a nice red aluminium box:

.. image:: ../galleries/apu2/apu2-front.jpg
   :alt: PC Engines APU2 front
   :target: ../galleries/apu2/apu2-front.jpg
   :class: center

Measuring
=========

Speed of the APU and K3s looks good so far: everything feels responsive.
Sysbench CPU benchmark shows that the APU is 6x faster than the Raspberry PI 3, at least when I replicated the `benchmark from geeks3d <https://www.geeks3d.com/20160322/tested-raspberry-pi-3-vs-raspberry-pi-2-cpu-and-gpu-benchmarks-burn-in-test/>`_.

.. code-block:: bash

    $ sysbench cpu run --threads=4 --events=10000 --cpu-max-prime=20000 --time=0 --validate
    sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

    CPU speed:
        events per second:   479.62

    General statistics:
        total time:                          20.8442s
        total number of events:              10000


For comparison: my T480s laptop with i5-8350U CPU takes ~5s for the same task (4x faster than the APU).


I measured the power consumption and it mostly shows around 6W when idle (K3s and kube-ops-view running):

.. image:: ../galleries/apu2/apu2-power-consumption.jpg
   :alt: Power consumption measurement
   :class: center

The CPU core temperature is constantly around 49-52° C (even when running sysbench), which sounds fine:

.. code-block:: bash

    $ sensors
    k10temp-pci-00c3
    Adapter: PCI adapter
    temp1:        +52.6°C  (high = +70.0°C)
                        (crit = +105.0°C, hyst = +104.0°C)

    fam15h_power-pci-00c4
    Adapter: PCI adapter
    power1:        8.21 W  (interval =   0.01 s, crit =   6.00 W)

K3s, Metrics Server, and kube-ops-view only take limited capacity (6% CPU and 400 MiB of 2 GiB used):

.. image:: ../galleries/apu2/kube-ops-view-node-usage.png
   :alt: kube-ops-view node usage
   :class: center


Summary
=======

I'm quite happy with the first results:

* installation via serial console was OK to deal with
* the board and enclosure make a quality impression
* speed (only 4x slower than my laptop) feels much better than the sluggish Raspi
* power consumption (6-9W) and temperature seem to be reasonable

All in all I now have a small Kubernetes home server which can run any amd64 Docker image and still has plenty of free capacity :-)

That's it for now! I will post news after playing more with the server and K3s_.


.. _PC Engines APU2 board: https://pcengines.ch/apu2.htm
.. _VARIA: https://www.varia-store.com/
.. _use the serial console: https://pcengines.ch/howto.htm#serialconsole
.. _K3s: https://k3s.io/
.. _Kubernetes Operational View: https://github.com/hjacobs/kube-ops-view

