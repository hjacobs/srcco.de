.. title: Electing a Works Council in Germany
.. slug: electing-works-council-germany
.. date: 2020/02/24 19:07:00
.. tags:
.. link:
.. description:
.. previewimage: ../galleries/electoral-board-legal-training.jpg
.. type: text

.. image:: ../galleries/electoral-board-legal-training.jpg
   :class: left

`Zalando SE is getting a works council (Betriebsrat) <https://www.morgenpost.de/berlin/article228419561/4500-Zalando-Mitarbeiter-waehlen-Betriebsrat.html>`_.
This blog post describes what I learned about the election process.


.. TEASER_END

DISCLAIMER: I'm not a lawyer and I'm not a subject matter expert, I left out details and some information in this blog post might be wrong!

I won't explain `what a works council is <https://en.wikipedia.org/wiki/Works_council#Germany>`_ and what rights and responsibilities it has.

Zalando SE is pretty big and the "simple" election mode does not apply to us,
i.e. the election will be a "list election" and follow the style of the German federal election (Bundestagswahl) with all its bells and whistles.

As there was no previous works council, the Electoral Board needed to be elected first by a general assembly.
This already happened end of last year. The current Electoral Board has 11 members and is tasked to organize the works council election.
I was elected as substitute and had the pleasure to join a two-day legal training.
This is what I learned.

The rough timeline for establishing a works council looks like this:

* inform Zalando colleagues about the works council and election
* compile the electoral list (Wählerliste)
* calculate minority seats
* publish the election declaration (①)
* gather submissions of nomination lists (submission deadline ②)
* announce nomination lists (③)
* prepare the ballots and polling stations
* perform the election (④)
* count the votes
* publish the results
* schedule the first meeting of the new works council

The time between election declaration (①) and submission deadline (②) is two weeks.
The minimum time between election declaration and election day (④)
is 6 weeks. On a horizontal timeline it might look like:

.. code-block:: text

    ①---|---②---③---|---|---|---④
    |<2 wks>|                    |
    |<--     min 6 weeks      -->|


Inform Zalando Colleagues
=========================

Zalando is very international and the concept of a works council (Betriebsrat) is very German.
To ensure that all colleagues understand the importance of the election and the modalities,
information campaigns need to be organized.

Compile the Electoral List
==========================

The electoral list contains all eligible voters, i.e. all Zalando employees older than 18 years who are not "executive staff".
The employer (Zalando) needs to provide the data upon request by the Electoral Board.
The list also needs to contain each person's gender.

Calculate Minority Seats
========================

The gender minority gets reserved seats in the works council.
The number of seats is calculated by the `D'Hondt method <https://en.wikipedia.org/wiki/D%27Hondt_method>`_.
As an example, let's take a company establishment with 106 men and 52 women:

==================  ===============
Men                 Women
==================  ===============
106 / 1 = 106 (1)   52 / 1 = 52 (3)
106 / 2 = 53  (2)   52 / 2 = 26 (6)
106 / 3 = 35.3 (3)  52 / 3 = 17.3
106 / 4 = 26.5 (4)  52 / 4 = 13
106 / 5 = 21.2 (5)  52 / 5 = 10.4
106 / 6 = 17.7      52 / 6 = 8.7
106 / 7 = 15.1      52 / 7 = 7.4
⋮                   ⋮
==================  ===============

I won't explain the method in detail here, but essentially you create first a table with numbers divided by 1, 2, 3, etc,
and then enumerate seats along the highest numbers (which are 106, 53, 52, and so on, jumping between the two columns).
In our example this leads to 5 seats for men and 2 for women. Only the minority gets reserved seats, so women would get at least 2 seats in the works council in this example.

It's unclear for me how this works with 3 genders (female, male, diverse).

Publish the Election Declaration
================================

The election declaration contains all important information: date and location(s) of the election,
the number of works council members, and the number of minority seats.
The number of works council members is calculated by the "normal" number of employees in the establishment and is defined by law:

=========== =============
Employees   Works Council
=========== =============
⋮           ⋮
2501-3000   21 members
3001-3500   23 members
3501-4000   25 members
4001-4500   27 members
4501-5000   29 members
⋮           ⋮
=========== =============


Publishing the declaration starts the clock and nomination lists need to be submitted after two weeks.

Nomination Lists
================

The works council election is a "list election",
i.e. any eligible employee can create a nomination list with candidates and voters only vote for one list (not for individual candidates).
Candidates must be in the company for at least 6 months and must be eligible voters (see "Electoral List" above).
The number of candidates per nomination list can be arbitrary, but all genders (esp. the minority gender) should be represented as candidates.
The order of candidates is important as only the first N candidates are elected, where N is depending on the number of votes (proportion) the lists gets
(see "Count the Votes" below).

Nomination lists need at least 50 supporting signatures and each employee can only support at most one list.

The formation of nomination lists and campaigning for them will be an interesting social experiment to observe.
Nomination lists could be created around topics or around roles (e.g. a list to represent "Product Managers in Zalando").
"Fun" list names or names representing a political party are not allowed.

Each candidate needs to be listed with full name, date of birth, and job role.
A nomination list could look like:

.. code-block:: text

    List name: Copy Writers in Zalando

    Candidates:

     1. Janine Vakha, 1980-01-01, Copy Writer Department X, <signature>
     2. Max Mustermann, 1984-10-27, Lead Copy Writer, <signature>
     3. Laura Smith, 1990-06-15, Copy Writer, <signature>
     4. John Doe, 1979-07-03, Copy Writer, <signature>

    Supporting Signatures:

     1. Fabiano Jun-Ho, <signature>
     2. …
     ⋮
    25. …
     ⋮
    50. …

The name of the list is optional and the names of the first two candidates will be used if no list name was provided.


Gather Submissions of Nomination Lists
======================================

The Electoral Board has to check each list:

* the list name has to be valid
* candidates must be eligible
* 50 supporting signatures have to be present

Each voter can only support one list, so cross-checking that supporting signatures don't appear twice is crucial.

Announcement of Nomination Lists
================================

The submission deadline is two weeks after the election declaration and nomination lists
are announced in the same way as the election declaration.
The order of lists is random (decided by drawing a lot).


Prepare the Ballots and Polling Stations
========================================

Polling stations need to be prepared:

* the election must be secret, i.e. appropriate measures must be taken
* at least one member of the Electoral Board must be present at each polling station at all times
* voters must only be able to vote once, i.e. some form of electronic list to mark voters who voted needs to be set up

Ballots contain the nomination lists in random order (determined by drawing a lot, see above),
each list's name ("keyword") and the first two candidates with full name and job title.
This could look like this (note that the first list has no name/keyword):

.. code-block:: text

    Works Council Election 2020

    [ ] Schmidt, Michael, Software Engineer; Townsend, Anna, Software Engineer
    [ ] "Copy Writers in Zalando" - Vekha, Janine, Copy Writer; Mustermann, Max, Lead Copy Writer
     ⋮
    [ ] …

Each voter only has one vote (can select one list).

Perform the Election
====================

The election probably will be more than one day and in multiple office locations to ensure that everybody can participate.
The actual voting procedure might look like this:

* go to your preferred polling station (office location)
* identify with official ID card or passport (will be checked against Electoral List)
* take paper ballot
* go to polling booth and mark "X for your desired list (you only have one vote)
* put the ballot into the polling box

Postal voting (Briefwahl) is also possible for people who cannot be present at one of the office locations.

Note that the election will be done with "classic" paper ballots. Electronic voting is not legally compliant (rechtssicher).

Count the Votes
===============

Votes are counted publicly.
Seats are allocated using the `D'Hondt method <https://en.wikipedia.org/wiki/D%27Hondt_method>`_.
If the highest numbers calculated for the nomination lists do not include the required minimum number of the minority gender (2 seats in our example above),
the person on the nomination list with the lowest number is replaced by a non-selected person of the minority gender.
This means that nomination lists should make sure to include enough candidates from all genders to avoid "giving away" a seat to a different list.

Here some example with only three lists, 150 votes in total, 2 reserved seats for women (f), and 7 seats in the works council:

==================== ==================== ===================
List A (80 votes)    List B (50 votes)    List C (20 votes)
==================== ==================== ===================
A1 (m) 80 / 1 = 80   B1 (m) 50 / 1 = 50   C1 (m) 20 / 1 = 20
A2 (f) 80 / 2 = 40   B2 (m) 50 / 2 = 25   C2 (f) 20 / 2 = 10
A3 (m) 80 / 3 = 26.7 B3 (f) 50 / 3 = 16.7 C3 (f) 20 / 3 = 6.7
A4 (m) 80 / 4 = 20   B4 (m) 50 / 4 = 12.5
A5 (m) 80 / 5 = 16   B5 (f) 50 / 5 = 50
==================== ==================== ===================

This would result in the election of candidates A1, B1, A2, B2, A3, C1, and B3.
The candidate A4 would have been elected, but has to give his seat to B3 as List A has no other female candidates.

Publish the Results
===================

The new works council becomes active as soon as the results are published.

Schedule First Meeting
======================

The last job of the Electoral Board is to invite to the first meeting of the new works council.

